#pragma once

#include <Windows.h>
#include <algorithm>
#include <vector>
#include <Psapi.h>
#include <TlHelp32.h>

#pragma  comment(lib, "psapi")

namespace WzL
{
	namespace Util
	{
		BOOL WriteMemory( const DWORD Address, std::vector< BYTE > &vBytes );
		BOOL WriteNOPs( const DWORD Address, UINT NOPAmount );
		BOOL GetMemoryBytes( const DWORD Address, const UINT Length, std::vector< BYTE > &vBytes );
		void JmpCall( bool Jmp, DWORD Source, LPVOID Destination, UINT NOPAmount );
		DWORD ScanForPattern( std::string Pattern, const DWORD Begin = 0x00400000, DWORD End = 0x7FFFFFFF );
		DWORD GetPointedAddress( ULONG_PTR* Base, INT Offset );

		void StrEraseChar( std::string &Str, const unsigned char Char );
		UINT StrCountChar( const std::string &Str, const char Char );
		short int HexCharToDec( const BYTE HexChar );
		std::string StrToUpper( std::string &Str );
		bool StrContainOnly( const std::string &Str, const std::string LimitStr  );
		bool ByteStrToVector( std::vector< BYTE > &Vect, std::string ByteStr );

		DWORD GetModuleSize( DWORD ProcessID, std::string ModuleStr );
	};

	class Cheat
	{
	public:
		DWORD	mAddress;
		bool	mIsNOP;
		UINT	mNOPAmount;
	private:
		std::vector< BYTE > mVEnableBytes;
		std::vector< BYTE > mVDisableBytes;
		bool mEnabled;
	public:
		Cheat();
		Cheat( DWORD Address, std::string EnableBytesStr, bool Enabled = false );
		Cheat( DWORD Address, UINT NOPAmount, bool Enabled = false );
		~Cheat();
	public:
		int SetToEnabled( bool EnableIt );
		inline bool IsEnabled();
		bool SetEnableBytesByStr( std::string EnableBytesStr );
		bool SetEnableBytesToNOP( UINT NOPAmount );
	};

	class Pointer
	{
	public:
		ULONG_PTR* mBase;
		std::vector< INT > mvOffsets;
	public:
		Pointer();
		Pointer( DWORD Base, INT Offset );
		Pointer( DWORD Base, UINT Level, ... );
		~Pointer();
	public:
		/**
		 * A function to read the pointer.
		 */
		template< typename T > T Read()
		{
			#pragma region Read
			if( this->mBase == NULL ) {
				return 0;
			} else if( this->mvOffsets.size() == 0 ) {
				return 0;
			} else if( IsBadReadPtr( ( LPVOID )this->mBase, sizeof( ULONG_PTR ) ) ) {
				return 0;
			}

			ULONG_PTR Ret = *( ULONG_PTR* )( ( *( ULONG_PTR* )this->mBase ) + this->mvOffsets[ 0 ] );

			if( mvOffsets.size() > 1 ) {
				for( UINT I = 1; I != mvOffsets.size(); ++ I ) {
					Ret = *( ULONG_PTR* )( Ret + mvOffsets[ I ] );
					if( IsBadReadPtr( ( LPVOID )( this->mBase + mvOffsets[ I ] ), sizeof( ULONG_PTR ) ) )
					{
						return 0;
					}
				}
			}
			return ( T )Ret;
			#pragma endregion Read
		};

		/**
		 * A function to write to the pointer.
		 * @ Val = The value that will be written to the pointer.
		 */
		template< typename T > BOOL Write( T Val ) {
			#pragma region Write
			if( this->mBase == NULL ) {
				return FALSE;
			} else if( this->mvOffsets.size() == 0 ) {
				return FALSE;
			} else if( IsBadReadPtr( ( LPVOID )this->mBase, sizeof( ULONG_PTR ) ) ) {
				return FALSE;
			}

			if( mvOffsets.size() > 1 ) {
				DWORD O = WzL::Util::GetPointedAddress( this->mBase, this->mvOffsets[ 0 ] );
				for( UINT I = 1; I != mvOffsets.size() - 1; ++ I ) {
					O =  WzL::Util::GetPointedAddress( ( LPDWORD )O, this->mvOffsets[ I ] );
				}
				*( LPDWORD )( *( LPDWORD( O ) ) + this->mvOffsets[ this->mvOffsets.size() - 1 ] ) = Val;
			}
			else {
				// single
			}
			return TRUE;
			#pragma endregion Write
		};
	};
};
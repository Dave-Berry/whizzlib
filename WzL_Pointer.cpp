#include "WhizzLib.h"

/**
 * Default Constructor.
 */
WzL::Pointer::Pointer()
	:	mBase( NULL )
	,	mvOffsets( 0 )
{
};

/**
 * Destructor.
 */
WzL::Pointer::~Pointer() {
};

/**
 * Single Pointer Constructor.
 */
WzL::Pointer::Pointer( DWORD Base, INT Offset )
	:	mBase( ( ULONG_PTR* )Base )
{
	mvOffsets.resize( 1 );
	mvOffsets[ 0 ] = Offset;
};

/**
 * Multi-level Pointer Constructor.
 */
WzL::Pointer::Pointer( DWORD Base, UINT Level, ... )
	:	mBase( ( ULONG_PTR* )Base )
{
	va_list vl;
	va_start( vl, Level );

	for( UINT I = 0; I != Level; ++ I ) {
		mvOffsets.push_back( va_arg( vl, INT ) );
	}

	va_end( vl );
};
#include "WhizzLib.h"

#include <algorithm>

/**
 * This function can write some bytes at a desired address.
 * @ Address = The address of where the bytes are suppose to be written.
 * @ vBytes = The vector that contains the bytes that are to be written.
 */
BOOL WzL::Util::WriteMemory( const DWORD Address, std::vector< BYTE > &vBytes )
{
	if( Address == NULL ) {
		return FALSE;
	} else if( vBytes.size() == 0 ) {
		return FALSE;
	}

	DWORD OldProtection = NULL;
	const SIZE_T WriteLength = vBytes.size() * sizeof( BYTE );

	VirtualProtect( ( LPVOID )Address, WriteLength, PAGE_EXECUTE_READWRITE, &OldProtection );
	std::memcpy( ( LPVOID )Address, &vBytes[ 0 ], WriteLength );
	VirtualProtect( ( LPVOID )Address, WriteLength, OldProtection, &OldProtection );

	return TRUE;
};

/**
 * This function can be used to write a certain amount of NOPs at an address.
 */
BOOL WzL::Util::WriteNOPs( const DWORD Address, UINT NOPAmount ) {
	if( Address == NULL ) {
		return FALSE;
	} else if( NOPAmount == 0 ) {
		return FALSE;
	}

	DWORD OldProtection = NULL;

	VirtualProtect( ( LPVOID )Address, NOPAmount, PAGE_EXECUTE_READWRITE, &OldProtection );
	std::memset( reinterpret_cast< LPVOID >( Address ), 0x90, NOPAmount );
	VirtualProtect( ( LPVOID )Address, NOPAmount, OldProtection, &OldProtection );

	return TRUE;
};

/**
 * This function can get a certain amount of bytes at an address and copy it into a vector of BYTE.
 * @ Address = The address of the bytes are going to be copied.
 * @ Length = The amount of bytes that are to be copied.
 * @ vBytes = The vector that will contain the copied bytes.
 */
BOOL WzL::Util::GetMemoryBytes( const DWORD Address, const UINT Length, std::vector< BYTE > &vBytes )
{
	if( Address == NULL ) {
		return FALSE;
	} else if( Length == 0 ) {
		return FALSE;
	}

	for( UINT I = 0; I <= Length; I ++ ) {
		vBytes.push_back( *( BYTE* )( Address + I ) );
	}

	return TRUE;
};

/**
 * A function that writes the jmp or call instruction at an address.
 * @ Jmp = Set to true, to jmp. If false, it will be call.
 * @ Source = The address that will have the jmp/call instruction
 * @ Destination = The address that will be jmp to or called.
 * @ NOPAmount = The amount of NOPs to write after the jmp/call.
 */
void WzL::Util::JmpCall( bool Jmp, DWORD Source, LPVOID Destination, UINT NOPAmount )
{
	DWORD OldProtection = NULL;

	VirtualProtect( ( LPVOID )Source, 16, PAGE_EXECUTE_READWRITE, &OldProtection );
	*reinterpret_cast< LPBYTE >( Source ) = ( Jmp ? 0xE9 : 0xE8 );
	*reinterpret_cast< PDWORD >( Source + 1 ) = reinterpret_cast< DWORD >( Destination ) - Source - 5;

	if( NOPAmount > 0 ) {
		WzL::Util::WriteNOPs( Source, NOPAmount );
	}

	VirtualProtect( ( LPVOID )Source, 16, OldProtection, &OldProtection );
};

/**
 * This function can be used to search for an address by scanning the memory
 * for a matching pattern (or array of bytes/AoB).
 * @ Begin = The first/beginning address to scan.
 * @ End = The maximum/ending address to scan for.
 */
DWORD WzL::Util::ScanForPattern( std::string Pattern, const DWORD Begin, DWORD End )
{
	if( Begin >= End ) {
		return NULL;
	} else if( Pattern.length() == 0 ) {
		return NULL;
	} else if( !WzL::Util::StrContainOnly( Pattern, "0123456789ABCDEFabcdef? " ) ) {
		return NULL;
	}

	DWORD MaxModuleSize = WzL::Util::GetModuleSize( GetCurrentProcessId(), "" );
	if( End > MaxModuleSize ) {
		End = MaxModuleSize;
	}

	WzL::Util::StrEraseChar( Pattern, ' ' );

	DWORD Address = Begin;
	bool Match = true;
	/* Scan */
	while( Address < End ) {
		for( UINT I = 0; I != Pattern.length(); I ++, Address ++ ) {
			if( Pattern[ I ] != '?'/* && Pattern[ I + 1 ] != '?' */ ) {
				if( *( BYTE* )( Address ) ==
					( BYTE )( ( WzL::Util::HexCharToDec( Pattern[ I ] ) * 16 ) +
					WzL::Util::HexCharToDec( Pattern[ I + 1 ] ) ) )
				{
					I += 1;
				} else {
					Address ++;
					Match = false;
					break;
				}
			}
		}

		if( Match ) {
			const SIZE_T CountToken = WzL::Util::StrCountChar( Pattern, '?' );
			return( Address -= ( ( Pattern.length() - CountToken )/2 ) + CountToken ); 
		}
		else if( Match == false ) {
			Match = true;
		}
	}

	return NULL;
};

/**
 * This function can get the pointed address of a pointer.
 */
DWORD WzL::Util::GetPointedAddress( ULONG_PTR* Base, INT Offset ) {
	__try {
		return *( DWORD* )Base + Offset;
	}
	__except ( EXCEPTION_EXECUTE_HANDLER ) {
		return NULL;
	}
}

/**
 * A function to remove a certain character from a std::string.
 */
void WzL::Util::StrEraseChar( std::string &Str, unsigned char Char ) {
	Str.erase( std::remove( Str.begin(), Str.end(), Char ), Str.end() );
};

/**
 * A function to get the frequency of a character within Str.
 */
UINT WzL::Util::StrCountChar( const std::string &Str, const char Char ) {
	UINT Ret = 0;

	for( UINT I = 0; I != Str.length(); ++ I ) {
		if( Str[ I ] == Char ) {
			Ret ++;
		}
	}

	return Ret;
};

/**
 * Convert a "hex character" (0~F) to decimal.
 */
short int WzL::Util::HexCharToDec( const BYTE HexChar ) {
	const static char HexCharTable[ 22 + 1 ] = "0123456789ABCDEFabcdef";

	for( UINT I = 0; I != 22; ++ I ) {
		if( HexChar == HexCharTable[ I ] ) {
			if( I >= 17 ) {
				I -= 6;
			}
			return I;
		}
	}

	return -1;
};

/**
 * Transform all characters within the string to uppercase.
 */
std::string WzL::Util::StrToUpper( std::string &Str ) {
	std::string Ret = Str;
	std::transform( Ret.begin(), Ret.end(), Ret.begin(), ::toupper);
	return Ret;
};

/**
 * Checks if Str only contains certain characters.
 * @ Str = The std::string to check.
 * @ LimitStr = The characters that are only allowed in Str.
 */
bool WzL::Util::StrContainOnly( const std::string &Str, const std::string LimitStr ) {
	return ( Str.size() > 2 &&
		Str.find_first_not_of( LimitStr, 2 ) == std::string::npos );
};

/**
 * Extract bytes from a std::string and add them into a vector.
 */
bool WzL::Util::ByteStrToVector( std::vector< BYTE > &Vect, std::string ByteStr )
{
	if( ByteStr.length() <= 1 ) {
		return false;
	}

	// ByteStr.erase( std::remove( ByteStr.begin(), ByteStr.end(), ' ' ), ByteStr.end() );
	WzL::Util::StrEraseChar( ByteStr, ' ' );

	if( ByteStr.length() % 2 != 0 ) {
		return false;
	}

	// ByteStr = WzL::Util::StrToUpper( ByteStr );
	if( !WzL::Util::StrContainOnly( ByteStr, "0123456789ABCDEFabcdef" ) ) {
		return false;
	}

	BYTE VectByte = NULL;
	Vect.reserve( ByteStr.length()/2 );

	for( UINT Index = 0; Index != ByteStr.length(); Index += 2 )
	{
		VectByte = WzL::Util::HexCharToDec( ByteStr[ Index ] ) * 16;
		VectByte += WzL::Util::HexCharToDec( ByteStr[ Index + 1 ] );
		Vect.push_back( VectByte );
	}

	return true;
};

/**
 * This function is used to get the size of a module within a process.
 */
DWORD WzL::Util::GetModuleSize( DWORD ProcessID, std::string ModuleStr ) {
	HANDLE SnapShotHandle = NULL;
	MODULEENTRY32 ME32;

	SnapShotHandle = CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, ProcessID );
	ME32.dwSize = sizeof( MODULEENTRY32 );

	if( Module32First( SnapShotHandle, &ME32 ) ) {
		while( Module32Next( SnapShotHandle, &ME32 ) ) {
			if( !std::strncmp( ( char* )ME32.szModule, ModuleStr.c_str(), 8 ) ) {
				CloseHandle( SnapShotHandle );
				return ( DWORD )ME32.modBaseSize;
			}
		}
	}

	CloseHandle( SnapShotHandle );
	return NULL;
};
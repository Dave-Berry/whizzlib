#include "WhizzLib.h"

/**
 * Default Constructor.
 */
WzL::Cheat::Cheat()
	:	mEnabled( false )
	,	mAddress( NULL )
	,	mVEnableBytes( 0 )
	,	mVDisableBytes( 0 )
	,	mIsNOP( false )
	,	mNOPAmount( 0 )
{
};

/**
 * Destructor.
 */
WzL::Cheat::~Cheat() {
};

/**
 * A constructor that allows you to set the address and enable bytes (by string).
 * @ Enabled = If true, it will enable. This is an optional param.
 */
WzL::Cheat::Cheat( DWORD Address, std::string EnableBytesStr, bool Enabled )
	: mAddress( Address )
	,	mIsNOP( false )
	,	mNOPAmount( 0 )
{
	if( this->mAddress != NULL ) {
		if( EnableBytesStr.length() > 0 ) {
			WzL::Util::ByteStrToVector( this->mVEnableBytes, EnableBytesStr );
			WzL::Util::GetMemoryBytes( Address, this->mVEnableBytes.size(), this->mVDisableBytes );
			this->SetToEnabled( Enabled );
		}
	}
};

WzL::Cheat::Cheat( DWORD Address, UINT NOPAmount, bool Enabled )
	: mAddress( Address )
{
	if( this->mAddress != NULL ) {
		if( NOPAmount > 0 ) {
			/*
			this->mVEnableBytes.resize( NOPAmount );
			std::fill( this->mVEnableBytes.begin(), this->mVEnableBytes.end(), 0x90 );
			*/
			this->mIsNOP = true;
			this->mNOPAmount = NOPAmount;
			WzL::Util::GetMemoryBytes( Address, this->mVEnableBytes.size(), this->mVDisableBytes );
			this->SetToEnabled( Enabled );
		}
	}
};

/**
 * A function that can enable or disable whatever Cheat.
 */
int WzL::Cheat::SetToEnabled( bool EnableIt )
{
	if( this->mAddress == NULL ) {
		return -1;
	} else if( this->mVEnableBytes.size() == 0 ) {
		return -2;
	} else if( this->mVDisableBytes.size() == 0 ) {
		return -3;
	} else if( this->mIsNOP && this->mNOPAmount == 0 ) {
		return -4;
	}
	
	if( EnableIt != this->mEnabled ) {
		if( EnableIt ) {
			if( !this->mIsNOP ) {
				WzL::Util::WriteMemory( this->mAddress, this->mVEnableBytes );
			} else {
				WzL::Util::WriteNOPs( this->mAddress, this->mNOPAmount );
			}
		} else { WzL::Util::WriteMemory( this->mAddress, this->mVDisableBytes ); }

		this->mEnabled = EnableIt;
	}

	return 0;
};

/**
 * This function returns the value of WzL::Cheat::mEnabled. If its true, then
 * the Cheat is enabled.
 */
inline bool WzL::Cheat::IsEnabled() {
	return this->mEnabled;
};

/**
 * This function is used to fill vector WzL::Cheat::mvEnableBytes with
 * some bytes, which were extracted from param EnableBytesStr.
 * @ EnableBytesStr = Example of usage, "0F 84".
 */
bool WzL::Cheat::SetEnableBytesByStr( std::string EnableBytesStr ) {
	if( EnableBytesStr.length() == 0 ) {
		return false;
	}

	WzL::Util::ByteStrToVector( this->mVEnableBytes, EnableBytesStr );
	return true;
};

bool WzL::Cheat::SetEnableBytesToNOP( UINT NOPAmount ) {
	if( NOPAmount == 0 ) {
		return false;
	}

	this->mIsNOP = true;
	this->mNOPAmount = NOPAmount;

	return true;
};